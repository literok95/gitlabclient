plugins {
    androidApplication()
    kotlinAndroid()
    kapt()
}

android {
    compileSdk = AppConfig.sdkVersion
    buildToolsVersion = AppConfig.buildToolsVersion

    defaultConfig {
        applicationId = AppConfig.applicationId
        testInstrumentationRunner = AppConfig.testInstrumentationRunner
        minSdk = AppConfig.minSdkVersion
        targetSdk = AppConfig.sdkVersion
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {

    implementation(project(":core"))
    implementation(project(":core-network"))
    implementation(project(":core-persistent"))
    implementation(project(":feature-auth"))
    implementation(project(":feature-user"))
    implementation(project(":feature-issues"))
    implementation(project(":feature-navscreen"))
    implementation(project(":feature-mergerequsets"))

    androidX()
    dagger()
    navigation()
    leakCanary()
    tests()
}