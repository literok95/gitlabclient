package com.stmlab.stmx.android.gitlabclient

import android.app.Application
import android.content.Context
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.stmlab.stmx.android.gitlabclient.di.AppComponent
import com.stmlab.stmx.android.gitlabclient.di.DaggerAppComponent

class GitlabClientApplication: Application() {
    private var _appComponent: AppComponent? = null
    val appComponent get() = checkNotNull(_appComponent) { "AppComponent not initialize" }

    override fun onCreate() {
        super.onCreate()
        Logger.addLogAdapter(AndroidLogAdapter())
        _appComponent = DaggerAppComponent.factory().create(applicationContext)
    }
}

val Context.appComponent: AppComponent
    get() = when (this) {
        is GitlabClientApplication -> appComponent
        else -> applicationContext.appComponent
    }
