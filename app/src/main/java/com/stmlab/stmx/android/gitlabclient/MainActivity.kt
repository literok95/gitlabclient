package com.stmlab.stmx.android.gitlabclient

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.stmlab.stmx.android.featureauth.AuthFeatureInteractor
import com.stmlab.stmx.android.featureuser.UserFeatureInteractor
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var authApiFeature: AuthFeatureInteractor
    @Inject
    lateinit var userFeatureInteractor: UserFeatureInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        applicationContext.appComponent.inject(this)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        if (navHostFragment is NavHostFragment) {
            if (authApiFeature.checkAuth()) {
                val bundle = collectNavResToBundle()
                setStartDestination(navHostFragment, R.id.navigation_navscreen, bundle)
            } else {
                setStartDestination(navHostFragment, R.id.navigation_auth)
            }
        }

        authApiFeature.onAuthSuccess {
            navigateNavScreen()
        }

        userFeatureInteractor.addSignOutListener {
            authApiFeature.signOut()
            navigateAuth()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        authApiFeature.onAuthSuccess(null)
    }

    private fun collectNavResToBundle(): Bundle {
        return bundleOf(
            "MENU_RES_ID_TAG" to R.menu.bottom_navigation_menu,
            "BOTTOM_NAV_GRAPH_RES_ID_TAG" to R.navigation.navigation_bottom
        )
    }

    private fun setStartDestination(
        navHostFragment: NavHostFragment,
        @IdRes startDestination: Int,
        bundle: Bundle? = null
    ) {
        val navController = navHostFragment.navController
        val graph = navController.navInflater.inflate(R.navigation.navigation_app)
        graph.setStartDestination(startDestination)
        if (bundle != null) navController.setGraph(graph, bundle) else navController.graph = graph
    }


    private fun navigateNavScreen() {
        val bundle = collectNavResToBundle()
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_global_navigation_navscreen, bundle)
    }

    private fun navigateAuth() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_global_navigation_auth)
    }
}