package com.stmlab.stmx.android.gitlabclient.di

import android.content.Context
import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.gitlabclient.MainActivity
import dagger.BindsInstance
import dagger.Component

@ApplicationScope
@Component(
    modules = [AppModule::class]
)
interface AppComponent {
    fun inject(app: MainActivity)
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

}