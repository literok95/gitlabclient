package com.stmlab.stmx.android.gitlabclient.di

import dagger.Module

@Module(
    includes = [
        CoreModule::class,
        AuthFeatureModule::class,
        UserFeatureModule::class,
        NavScreenFeatureModule::class
    ]
)
class AppModule {
}