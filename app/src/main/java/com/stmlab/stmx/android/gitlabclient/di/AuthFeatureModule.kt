package com.stmlab.stmx.android.gitlabclient.di

import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.corenetwork.AuthApi
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.di.CoreNetworkApi
import com.stmlab.stmx.android.corepersistent.Preferences
import com.stmlab.stmx.android.corepersistent.di.PersistentPreferencesApi
import com.stmlab.stmx.android.featureauth.AuthFeatureInteractor
import com.stmlab.stmx.android.featureauth.di.AuthFeatureApi
import com.stmlab.stmx.android.featureauth.di.AuthFeatureComponentHolder
import com.stmlab.stmx.android.featureauth.di.AuthFeatureDependencies
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
class AuthFeatureModule {

    @ApplicationScope
    @Provides
    fun provideAuthFeatureDependencies(
        coreNetworkApi: CoreNetworkApi,
        persistentPreferencesApi: PersistentPreferencesApi
    ): AuthFeatureDependencies {
        return object : AuthFeatureDependencies {
            override fun networkAuthApi(): AuthApi {
                return coreNetworkApi.provideAuthApi()
            }

            override fun networkInteractor(): NetworkInteractor {
                return coreNetworkApi.provideNetworkInteractor()
            }

            override fun persistentPreferencesApi(): Preferences {
                return persistentPreferencesApi.providePreferences()
            }

            override fun dispatcherIO(): CoroutineDispatcher {
                return Dispatchers.IO
            }
        }
    }

    @ApplicationScope
    @Provides
    fun provideAuthFeatureApi(
        authFeatureDependencies: AuthFeatureDependencies
    ): AuthFeatureApi {
        AuthFeatureComponentHolder.init(authFeatureDependencies)
        return AuthFeatureComponentHolder.get()
    }

    @ApplicationScope
    @Provides
    fun provideAuthFeatureInteractor(authFeatureApi: AuthFeatureApi): AuthFeatureInteractor {
        return authFeatureApi.provideAuthFeatureInteractor()
    }
}