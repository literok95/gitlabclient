package com.stmlab.stmx.android.gitlabclient.di

import android.content.Context
import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.corenetwork.di.CoreNetworkApi
import com.stmlab.stmx.android.corenetwork.di.NetworkComponentHolder
import com.stmlab.stmx.android.corepersistent.di.PersistentComponentHolder
import com.stmlab.stmx.android.corepersistent.di.PersistentPreferencesApi
import dagger.Module
import dagger.Provides

@Module
class CoreModule {
    @ApplicationScope
    @Provides
    fun provideNetworkAuthApi(): CoreNetworkApi {
        NetworkComponentHolder.init()
        return NetworkComponentHolder.get()
    }

    @ApplicationScope
    @Provides
    fun providePersistentPreferencesApi(context: Context): PersistentPreferencesApi {
        PersistentComponentHolder.init(context)
        return PersistentComponentHolder.get()
    }
}