package com.stmlab.stmx.android.gitlabclient.di

import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.UserApi
import com.stmlab.stmx.android.corenetwork.di.CoreNetworkApi
import com.stmlab.stmx.android.corepersistent.di.PersistentPreferencesApi
import com.stmlab.stmx.android.featureuser.UserFeatureInteractor
import com.stmlab.stmx.android.featureuser.di.UserFeatureApi
import com.stmlab.stmx.android.featureuser.di.UserFeatureComponentHolder
import com.stmlab.stmx.android.featureuser.di.UserFeatureDependencies
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
class UserFeatureModule {
    @Provides
    fun provideUserFeatureDependencies(
        coreNetworkApi: CoreNetworkApi,
        persistentPreferencesApi: PersistentPreferencesApi
    ): UserFeatureDependencies {
        return object : UserFeatureDependencies {
            override fun networkUserApi(): UserApi {
                return coreNetworkApi.provideUserApi()
            }

            override fun networkInteractor(): NetworkInteractor {
                return coreNetworkApi.provideNetworkInteractor()
            }

            override fun dispatcherIO(): CoroutineDispatcher {
                return Dispatchers.IO
            }
        }
    }

    @Provides
    fun provideUserFeatureApi(userFeatureDependencies: UserFeatureDependencies): UserFeatureApi {
        UserFeatureComponentHolder.init(userFeatureDependencies)
        return UserFeatureComponentHolder.get()
    }

    @Provides
    fun provideUserFeatureInteractor(authFeatureApi: UserFeatureApi): UserFeatureInteractor {
        return authFeatureApi.provideUserFeatureInteractor()
    }
}