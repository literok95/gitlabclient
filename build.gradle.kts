buildscript {
    val kotlin_version by extra("1.5.20")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath(Plugins.androidGradlePlugin)
        classpath(Plugins.kotlinGradlePlugin)
        classpath(Plugins.gradleVersionPlugin)
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
    }
}

allprojects {
    apply(plugin = Plugins.gradleVersion)
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
