import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.plugin.use.PluginDependenciesSpec

const val kotlinVersion = "1.5.21"

object Plugins {

    private object Versions {
        const val buildTools = "4.2.2"
        const val gradleVersion = "0.39.0"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.buildTools}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    const val gradleVersionPlugin =
        "com.github.ben-manes:gradle-versions-plugin:${Versions.gradleVersion}"
    const val androidApplication = "com.android.application"
    const val androidLibrary = "com.android.library"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinParcelize = "kotlin-parcelize"
    const val gradleVersion = "com.github.ben-manes.versions"
    const val kapt = "kotlin-kapt"

}

object AppConfig {
    private object DefaultValue {
        const val appId = "27bef0a8dfa7def91c22a39db9da14fa43a7b3c4349f934b33a8ee06889073f5"
        const val appSecret = "074a92d997ce66a4841b92c7c811d9443ed4c180de6d12f2218dbafa22a62014"
        const val callbackUri = "app://com.stmlab.stmx.android.gitclient/"
        const val scopes = "api"
    }

    const val sdkVersion = 30
    const val minSdkVersion = 21
    const val buildToolsVersion = "30.0.3"
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val applicationId = "com.stmlab.stmx.android.gitlabclient"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    val appId: String = System.getenv("GIT_APP_ID") ?: DefaultValue.appId
    val appSecret: String = System.getenv("GIT_APP_SECRET") ?: DefaultValue.appSecret
    val callbackUri: String = System.getenv("GIT_CALLBACK_URI") ?: DefaultValue.callbackUri
    val scopes: String = System.getenv("GIT_SCOPES") ?: DefaultValue.scopes
}

object Types {
    const val string = "String"
}

object Libraries {

    object Versions {
        const val gson = "2.8.7"
        const val coroutines = "1.5.0"
        const val logger = "2.2.0"
        const val dagger = "2.35.1"
        const val retrofit = "2.9.0"
        const val okHttp = "4.9.0"
        const val androidXPreferences = "1.1.1"
        const val androidXFragment = "1.3.5"
        const val navigation = "2.4.0-alpha04"
        const val androidXCore = "1.6.0"
        const val androidXAppCompat = "1.3.0"
        const val androidXAnnotation = "1.2.0"
        const val material = "1.3.0"
        const val constraintLayout = "2.0.4"
        const val lifecycle = "2.3.1"
        const val glide = "4.12.0"
        const val room = "2.3.0"
        const val leakCanary = "2.7"
    }

    const val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val kotlinCoroutinesAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val logger = "com.orhanobut:logger:${Versions.logger}"
    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val jUnit = "junit:junit:4.13.2"
    const val androidTestJUnit = "androidx.test.ext:junit:1.1.3"
    const val androidTestEspresso = "androidx.test.espresso:espresso-core:3.4.0"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val converterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val converterScalars = "com.squareup.retrofit2:converter-scalars:${Versions.retrofit}"
    const val okHttpClient = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    const val androidXPreferences =
        "androidx.preference:preference-ktx:${Versions.androidXPreferences}"
    const val androidXFragment = "androidx.fragment:fragment-ktx:${Versions.androidXFragment}"
    const val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val navigationFragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val androidXCore = "androidx.core:core-ktx:${Versions.androidXCore}"
    const val androidXAppCompat = "androidx.appcompat:appcompat:${Versions.androidXAppCompat}"
    const val androidXAnnotation = "androidx.annotation:annotation:${Versions.androidXAnnotation}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val lifecycleLiveData =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val lifecycleViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanary}"
}

fun DependencyHandler.kotlinStdlib() {
    api(Libraries.kotlinStdlib)
}

fun DependencyHandler.coroutines() {
    api(Libraries.kotlinCoroutinesAndroid)
}

fun DependencyHandler.logger() {
    api(Libraries.logger)
}

fun DependencyHandler.gson() {
    implementation(Libraries.gson)
}

fun DependencyHandler.dagger() {
    implementation(Libraries.dagger)
    kapt(Libraries.daggerCompiler)
}

fun DependencyHandler.network() {
    implementation(Libraries.retrofit)
    implementation(Libraries.converterGson)
    implementation(Libraries.converterScalars)
    implementation(Libraries.okHttpClient)
}

fun DependencyHandler.preferences() {
    implementation(Libraries.androidXPreferences)
}

fun DependencyHandler.fragment() {
    implementation(Libraries.androidXFragment)
}

fun DependencyHandler.navigation() {
    implementation(Libraries.navigationUI)
    implementation(Libraries.navigationFragment)
}

fun DependencyHandler.androidX() {
    implementation(Libraries.androidXCore)
    implementation(Libraries.androidXAppCompat)
    implementation(Libraries.androidXAnnotation)
}

fun DependencyHandler.ui() {
    implementation(Libraries.material)
    implementation(Libraries.constraintLayout)
}

fun DependencyHandler.lifecycle() {
    implementation(Libraries.lifecycleLiveData)
    implementation(Libraries.lifecycleViewModel)
}

fun DependencyHandler.glide() {
    implementation(Libraries.glide)
    annotationProcessor(Libraries.glideCompiler)
}

fun DependencyHandler.room() {
    implementation(Libraries.room)
    implementation(Libraries.roomKtx)
    annotationProcessor(Libraries.roomCompiler)
}

fun DependencyHandler.leakCanary() {
    debugImplementation(Libraries.leakCanary)
}

fun DependencyHandler.tests() {
    testImplementation(Libraries.jUnit)
    androidTestImplementation(Libraries.androidTestJUnit)
    androidTestImplementation(Libraries.androidTestEspresso)
}

private fun DependencyHandler.implementation(depName: String) {
    add("implementation", depName)
}

private fun DependencyHandler.kapt(depName: String) {
    add("kapt", depName)
}

private fun DependencyHandler.compileOnly(depName: String) {
    add("compileOnly", depName)
}

private fun DependencyHandler.annotationProcessor(depName: String) {
    add("annotationProcessor", depName)
}

private fun DependencyHandler.api(depName: String) {
    add("api", depName)
}

private fun DependencyHandler.debugImplementation(depName: String) {
    add("debugImplementation", depName)
}

private fun DependencyHandler.testImplementation(depName: String) {
    add("testImplementation", depName)
}

private fun DependencyHandler.androidTestImplementation(depName: String) {
    add("androidTestImplementation", depName)
}

fun PluginDependenciesSpec.androidApplication() {
    id(Plugins.androidApplication)
}

fun PluginDependenciesSpec.androidLibrary() {
    id(Plugins.androidLibrary)
}

fun PluginDependenciesSpec.kotlinAndroid() {
    id(Plugins.kotlinAndroid)
}

fun PluginDependenciesSpec.parcelize() {
    id(Plugins.kotlinParcelize)
}

fun PluginDependenciesSpec.kapt() {
    id(Plugins.kapt)
}