package com.stmlab.stmx.android.corenetwork

import com.stmlab.stmx.android.corenetwork.models.TokensDto
import com.stmlab.stmx.android.corenetwork.models.TokenInfoDto
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthApi {
    @POST("/oauth/token")
    suspend fun getAccessToken(
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("code") code: String,
        @Query("grant_type") grantType: String,
        @Query("redirect_uri") redirectUri: String
    ): TokensDto

    @GET("/oauth/token/info")
    suspend fun getTokenInfo(): TokenInfoDto

    @GET("/oauth/token/info")
    suspend fun getTokenInfo(
        @Query("access_token") accessToken: String
    ): TokenInfoDto
}