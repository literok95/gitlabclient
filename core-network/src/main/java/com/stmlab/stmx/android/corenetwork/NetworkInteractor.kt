package com.stmlab.stmx.android.corenetwork

import com.stmlab.stmx.android.corenetwork.interceptors.AccessTokenInteractor
import com.stmlab.stmx.android.corenetwork.interceptors.GitHostInteractor

interface NetworkInteractor : GitHostInteractor, AccessTokenInteractor