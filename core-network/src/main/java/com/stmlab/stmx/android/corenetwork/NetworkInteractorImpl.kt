package com.stmlab.stmx.android.corenetwork

import com.stmlab.stmx.android.corenetwork.interceptors.AccessTokenInterceptor
import com.stmlab.stmx.android.corenetwork.interceptors.GitHostInteractor

internal class NetworkInteractorImpl(
    private val accessTokenInterceptor: AccessTokenInterceptor,
    private val gitHostInterceptor: GitHostInteractor
) : NetworkInteractor {
    override fun setHost(gitHost: String) {
        gitHostInterceptor.setHost(gitHost)
    }

    override fun setDefaultGitHost() {
        gitHostInterceptor.setDefaultGitHost()
    }

    override fun setAccessToken(accessToken: String, isOAuth: Boolean) {
        accessTokenInterceptor.setAccessToken(accessToken, isOAuth)
    }

    override fun clearAccessToken() {
        accessTokenInterceptor.clearAccessToken()
    }
}