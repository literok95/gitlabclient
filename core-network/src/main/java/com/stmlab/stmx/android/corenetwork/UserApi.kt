package com.stmlab.stmx.android.corenetwork

import com.stmlab.stmx.android.corenetwork.models.UserDto
import retrofit2.http.GET

interface UserApi {
    @GET("api/v4/user")
    suspend fun getCurrentUser(): UserDto
}