package com.stmlab.stmx.android.corenetwork.di

import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.corenetwork.AuthApi
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.UserApi
import dagger.Component

@ApplicationScope
@Component(
    modules = [NetworkModule::class]
)
internal interface NetworkComponent : CoreNetworkApi {
    @Component.Factory
    interface Factory {
        fun create(): NetworkComponent
    }
}

interface CoreNetworkApi {
    fun provideAuthApi(): AuthApi
    fun provideUserApi(): UserApi
    fun provideNetworkInteractor(): NetworkInteractor
}