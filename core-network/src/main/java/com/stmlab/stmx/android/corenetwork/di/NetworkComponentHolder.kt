package com.stmlab.stmx.android.corenetwork.di

object NetworkComponentHolder {
    private var networkComponentHolder: NetworkComponent? = null

    fun init() {
        if (networkComponentHolder == null) {
            synchronized(NetworkComponentHolder::class.java) {
                if (networkComponentHolder == null) {
                    networkComponentHolder = DaggerNetworkComponent.factory().create()
                }
            }
        }
    }

    fun get(): CoreNetworkApi {
        return checkNotNull(networkComponentHolder) { "NetworkComponent was not initialized!" }
    }

    fun reset() {
        networkComponentHolder = null
    }
}