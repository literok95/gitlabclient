package com.stmlab.stmx.android.corenetwork.di

import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.corenetwork.AuthApi
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.NetworkInteractorImpl
import com.stmlab.stmx.android.corenetwork.UserApi
import com.stmlab.stmx.android.corenetwork.interceptors.AccessTokenInterceptor
import com.stmlab.stmx.android.corenetwork.interceptors.AccessTokenInterceptorImpl
import com.stmlab.stmx.android.corenetwork.interceptors.GitHostInterceptor
import com.stmlab.stmx.android.corenetwork.interceptors.GitHostInterceptorImpl
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
internal class NetworkModule {

    private companion object {
        private const val DEFAULT_HOST = "gitlab.com"
    }

    @ApplicationScope
    @Provides
    fun provideAccessTokenInterceptor(): AccessTokenInterceptor {
        return AccessTokenInterceptorImpl()
    }

    @ApplicationScope
    @Provides
    fun provideGitHostInterceptor(): GitHostInterceptor {
        return GitHostInterceptorImpl(DEFAULT_HOST)
    }

    @ApplicationScope
    @Provides
    fun provideLoggerInterceptor(): Interceptor {
        return HttpLoggingInterceptor()
    }

    @ApplicationScope
    @Provides
    fun provideOkHttpClient(
        loggerInterceptor: Interceptor,
        accessTokenInterceptor: AccessTokenInterceptor,
        gitHostInterceptor: GitHostInterceptor,
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggerInterceptor)
            .addInterceptor(accessTokenInterceptor)
            .addInterceptor(gitHostInterceptor)
            .build()
    }

    @ApplicationScope
    @Provides
    fun provideNetworkInteractor(
        accessTokenInterceptor: AccessTokenInterceptor,
        gitHostInterceptor: GitHostInterceptor,
    ): NetworkInteractor {
        return NetworkInteractorImpl(accessTokenInterceptor, gitHostInterceptor)
    }

    @ApplicationScope
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://$DEFAULT_HOST/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @ApplicationScope
    @Provides
    fun provideAuthApi(retrofit: Retrofit): AuthApi {
        return retrofit.create(AuthApi::class.java)
    }

    @ApplicationScope
    @Provides
    fun provideUserApi(retrofit: Retrofit): UserApi {
        return retrofit.create(UserApi::class.java)
    }

}