package com.stmlab.stmx.android.corenetwork.interceptors

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

internal class AccessTokenInterceptorImpl : AccessTokenInterceptor {

    private var accessToken: String? = null
    private var contract: HeaderContract = HeaderContract.BEARER

    override fun setAccessToken(accessToken: String, isOAuth: Boolean) {
        if (this.accessToken == null || this.accessToken != accessToken) {
            contract = if (isOAuth) HeaderContract.BEARER else HeaderContract.TOKEN
            this.accessToken = accessToken
        }
    }

    override fun clearAccessToken() {
        accessToken = null
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = if (!accessToken.isNullOrBlank()) {
            updateRequest(chain)
        } else {
            chain.request()
        }
        return chain.proceed(request)
    }

    private fun updateRequest(chain: Interceptor.Chain): Request {
        return chain.request()
            .newBuilder()
            .addHeader(contract.key, contract.prefix + accessToken)
            .build()
    }

    enum class HeaderContract(val key: String, val prefix: String) {
        BEARER("Authorization", "Bearer "),
        TOKEN("PRIVATE-TOKEN", "")
    }
}

interface AccessTokenInterceptor : AccessTokenInteractor, Interceptor

interface AccessTokenInteractor {
    fun setAccessToken(accessToken: String, isOAuth: Boolean = false)
    fun clearAccessToken()
}