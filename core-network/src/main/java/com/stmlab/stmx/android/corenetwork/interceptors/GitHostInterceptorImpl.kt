package com.stmlab.stmx.android.corenetwork.interceptors

import okhttp3.Interceptor
import okhttp3.Response

internal class GitHostInterceptorImpl(
    private val defaultHost: String
) : GitHostInterceptor {

    private var gitHost: String = defaultHost
    private var prevGitHost: String = gitHost

    override fun setHost(gitHost: String) {
        if (this.gitHost != gitHost) this.gitHost = gitHost
    }

    override fun setDefaultGitHost() {
        gitHost = defaultHost
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = if (prevGitHost != gitHost) {
            prevGitHost = gitHost
            val originUrl = chain.request().url
            val url = chain.request().url.newBuilder()
                .scheme(originUrl.scheme)
                .host(gitHost)
                .port(originUrl.port)
                .build()
            chain.request().newBuilder()
                .url(url)
                .build()
        } else {
            chain.request()
        }
        return chain.proceed(request)
    }
}

interface GitHostInterceptor : GitHostInteractor, Interceptor

interface GitHostInteractor {
    fun setHost(gitHost: String)
    fun setDefaultGitHost()
}