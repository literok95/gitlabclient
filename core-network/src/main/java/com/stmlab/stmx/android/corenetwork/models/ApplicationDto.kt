package com.stmlab.stmx.android.corenetwork.models

import com.google.gson.annotations.SerializedName

data class ApplicationDto(
    @SerializedName("uid") val uid: String? = null
)