package com.stmlab.stmx.android.corenetwork.models

import com.google.gson.annotations.SerializedName

data class IdentitiesItemDto(
    @SerializedName("provider") val provider: String? = null,
    @SerializedName("extern_uid") val externUid: String? = null
)