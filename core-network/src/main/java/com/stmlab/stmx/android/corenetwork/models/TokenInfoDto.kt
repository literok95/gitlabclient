package com.stmlab.stmx.android.corenetwork.models

import com.google.gson.annotations.SerializedName

data class TokenInfoDto(
    @SerializedName("resource_owner_id") val resourceOwnerId: Int? = null,
    @SerializedName("application") val applicationDto: ApplicationDto? = null,
    @SerializedName("scope") val scope: List<String>? = null,
    @SerializedName("created_at") val createdAt: Int? = null,
    @SerializedName("expires_in") val expiresIn: Any? = null
)