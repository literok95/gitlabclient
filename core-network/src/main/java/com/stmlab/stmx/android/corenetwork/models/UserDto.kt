package com.stmlab.stmx.android.corenetwork.models

import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("can_create_project") val canCreateProject: Boolean? = null,
    @SerializedName("private_profile") val privateProfile: Boolean? = null,
    @SerializedName("theme_id") val themeId: Int? = null,
    @SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("bio") val bio: String? = null,
    @SerializedName("projects_limit") val projectsLimit: Int? = null,
    @SerializedName("linkedin") val linkedin: String? = null,
    @SerializedName("last_activity_on") val lastActivityOn: String? = null,
    @SerializedName("can_create_group") val canCreateGroup: Boolean? = null,
    @SerializedName("skype") val skype: String? = null,
    @SerializedName("twitter") val twitter: String? = null,
    @SerializedName("identities") val identities: List<IdentitiesItemDto>? = null,
    @SerializedName("last_sign_in_at") val lastSignInAt: String? = null,
    @SerializedName("color_scheme_id") val colorSchemeId: Int? = null,
    @SerializedName("id") val id: Int? = null,
    @SerializedName("state") val state: String? = null,
    @SerializedName("confirmed_at") val confirmedAt: String? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("current_sign_in_at") val currentSignInAt: String? = null,
    @SerializedName("two_factor_enabled") val twoFactorEnabled: Boolean? = null,
    @SerializedName("external") val external: Boolean? = null,
    @SerializedName("avatar_url") val avatarUrl: String? = null,
    @SerializedName("web_url") val webUrl: String? = null,
    @SerializedName("website_url") val websiteUrl: String? = null,
    @SerializedName("bio_html") val bioHtml: String? = null,
    @SerializedName("organization") val organization: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("location") val location: String? = null,
    @SerializedName("public_email") val publicEmail: String? = null,
    @SerializedName("username") val username: String? = null
)