package com.stmlab.stmx.android.corepersistent

import com.stmlab.stmx.android.core.models.Account

interface Preferences {
    var currentAccount: Account?
}