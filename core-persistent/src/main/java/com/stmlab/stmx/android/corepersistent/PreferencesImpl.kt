package com.stmlab.stmx.android.corepersistent

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.stmlab.stmx.android.core.getParcelable
import com.stmlab.stmx.android.core.models.Account
import com.stmlab.stmx.android.core.putParcelable
import com.stmlab.stmx.android.core.scopes.ApplicationScope
import javax.inject.Inject

@ApplicationScope
internal class PreferencesImpl @Inject constructor(context: Context) : Preferences {

    private companion object {
        private const val ACCOUNT_KEY = "com.stmlab.stmx.android.persistent.account"
    }

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(context)
    }

    override var currentAccount: Account?
        get() = sharedPreferences.getParcelable(ACCOUNT_KEY, null)
        set(value) {
            sharedPreferences.edit {
                if (value != null) putParcelable(ACCOUNT_KEY, value) else remove(ACCOUNT_KEY)
            }
        }

}