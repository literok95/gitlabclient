package com.stmlab.stmx.android.corepersistent.di

import android.content.Context
import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.corepersistent.Preferences
import dagger.BindsInstance
import dagger.Component

@ApplicationScope
@Component(
    modules = [PreferencesModule::class]
)
internal interface PersistentComponent : PersistentPreferencesApi {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): PersistentComponent
    }
}

interface PersistentPreferencesApi {
    fun providePreferences(): Preferences
}