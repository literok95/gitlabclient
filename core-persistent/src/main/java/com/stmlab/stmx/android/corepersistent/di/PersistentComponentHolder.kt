package com.stmlab.stmx.android.corepersistent.di

import android.content.Context
import android.util.Log

object PersistentComponentHolder {
    private var persistentComponentHolder: PersistentComponent? = null

    fun init(context: Context) {
        if (persistentComponentHolder == null) {
            synchronized(PersistentComponentHolder::class.java) {
                if (persistentComponentHolder == null) {
                    Log.i("PurchaseCH", "initAndGet()")
                    persistentComponentHolder = DaggerPersistentComponent.factory().create(context)
                }
            }
        }
    }

    fun get(): PersistentPreferencesApi {
        return checkNotNull(persistentComponentHolder) { "PersistentComponent was not initialized!" }
    }

    fun reset() {
        persistentComponentHolder = null
    }
}