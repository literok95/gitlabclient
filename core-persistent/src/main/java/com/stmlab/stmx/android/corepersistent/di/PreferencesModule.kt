package com.stmlab.stmx.android.corepersistent.di

import com.stmlab.stmx.android.core.scopes.ApplicationScope
import com.stmlab.stmx.android.corepersistent.Preferences
import com.stmlab.stmx.android.corepersistent.PreferencesImpl
import dagger.Binds
import dagger.Module

@Module
internal interface PreferencesModule {

    @Binds
    @ApplicationScope
    fun providePreferences(preferencesImpl: PreferencesImpl): Preferences
}