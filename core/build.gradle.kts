plugins {
    androidLibrary()
    kotlinAndroid()
    parcelize()
    kapt()
}

android {
    compileSdk = AppConfig.sdkVersion
    buildToolsVersion = AppConfig.buildToolsVersion

    defaultConfig {
        minSdk = AppConfig.minSdkVersion
        targetSdk = AppConfig.sdkVersion
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    kotlinStdlib()
    gson()
    coroutines()
    logger()
    dagger()
    tests()
}