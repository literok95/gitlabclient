package com.stmlab.stmx.android.core

interface BaseHolder<C, D> {
    fun init(dependencies: D)
    fun get(): C
    fun reset()
}