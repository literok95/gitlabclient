package com.stmlab.stmx.android.core

import com.orhanobut.logger.Logger
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

abstract class BaseRepository(
    private val coroutineDispatcher: CoroutineDispatcher
) {
    suspend fun <T, V> makeRequest(map: T.() -> V, request: suspend () -> T): BaseResult<V> {
        return withContext(coroutineDispatcher) {
            try {
                val resultDto = request.invoke()
                Logger.t("BASE_REPO")
                Logger.d("network result success")
                val result = resultDto.map()
                Logger.t("BASE_REPO")
                Logger.d("mapping success")
                BaseResult.Success(result)
            } catch (exception: Exception) {
                Logger.t("BASE_REPO")
                Logger.e("exception: $exception")
                BaseResult.Error
            }
        }
    }
}