package com.stmlab.stmx.android.core

sealed class BaseResult<out T> {
    data class Success<out T>(val body: T) : BaseResult<T>()
    object Error : BaseResult<Nothing>()
}