package com.stmlab.stmx.android.core.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Account(
    val accessToken: String,
    val refreshToken: String,
    val host: String,
    val isOAuth: Boolean
) : Parcelable