package com.stmlab.stmx.android.core.models

data class AuthConfig(
    val gitHost: String,
    val appId: String,
    val clientSecret: String,
    val callbackUri: String,
    val state: Int,
    val requestScope: String
){
    fun toUrl(): String {
        return "https://${gitHost}/oauth/authorize?client_id=$appId&redirect_uri=$callbackUri&response_type=code&state=$state&scope=$requestScope"
    }
}