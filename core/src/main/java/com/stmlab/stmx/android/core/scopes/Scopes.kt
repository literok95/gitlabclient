package com.stmlab.stmx.android.core.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AuthFeatureScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class UserFeatureScope