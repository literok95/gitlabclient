plugins {
    androidLibrary()
    kotlinAndroid()
    kapt()
}

android {
    compileSdk = AppConfig.sdkVersion
    buildToolsVersion = AppConfig.buildToolsVersion

    defaultConfig {
        minSdk = AppConfig.minSdkVersion
        targetSdk = AppConfig.sdkVersion
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        buildConfigField(Types.string, "APP_ID", "\"${AppConfig.appId}\"")
        buildConfigField(Types.string, "APP_SECRET", "\"${AppConfig.appSecret}\"")
        buildConfigField(Types.string, "CALLBACK_URI", "\"${AppConfig.callbackUri}\"")
        buildConfigField(Types.string, "SCOPES", "\"${AppConfig.scopes}\"")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }
}

dependencies {
    implementation(project(":core"))
    implementation(project(":core-network"))
    implementation(project(":core-persistent"))
    androidX()
    ui()
    fragment()
    lifecycle()
    navigation()
    dagger()
    tests()
}