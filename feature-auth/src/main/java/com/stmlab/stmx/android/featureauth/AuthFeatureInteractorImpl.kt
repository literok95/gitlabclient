package com.stmlab.stmx.android.featureauth

import android.webkit.CookieManager
import com.stmlab.stmx.android.core.scopes.AuthFeatureScope
import com.stmlab.stmx.android.featureauth.repository.AuthRepository
import javax.inject.Inject

@AuthFeatureScope
internal class AuthFeatureInteractorImpl @Inject constructor(
    private val authRepository: AuthRepository
) : AuthFeatureInteractor {

    override fun onAuthSuccess(callback: (() -> Unit)?) {
        authRepository.setOnAuthSuccessListener(callback)
    }

    override fun signOut() {
        CookieManager.getInstance().removeAllCookies(null)
        authRepository.signOut()
    }

    override fun checkAuth(): Boolean {
        return authRepository.checkAuth()
    }

}

@AuthFeatureScope
interface AuthFeatureInteractor {
    fun onAuthSuccess(callback: (() -> Unit)?)
    fun signOut()
    fun checkAuth(): Boolean
}