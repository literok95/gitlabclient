package com.stmlab.stmx.android.featureauth

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment

class AuthFragment : Fragment(R.layout.fragment_auth) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.button_sign_in_via_access_token).setOnClickListener {
            Interactor.start()
        }
    }
}