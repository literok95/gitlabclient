package com.stmlab.stmx.android.featureauth

object Interactor {
    private var listener: (()-> Unit)? = null

    fun setListener(listener: (()-> Unit)?) {
        this.listener = listener
    }

    internal fun start() {
        listener?.invoke()
    }
}