package com.stmlab.stmx.android.featureauth.di

import com.stmlab.stmx.android.core.di.IoDispatcher
import com.stmlab.stmx.android.core.scopes.AuthFeatureScope
import com.stmlab.stmx.android.featureauth.AuthFeatureInteractor
import com.stmlab.stmx.android.featureauth.ui.AuthFragment
import com.stmlab.stmx.android.featureauth.ui.AuthViewModel
import com.stmlab.stmx.android.corenetwork.AuthApi
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corepersistent.Preferences
import dagger.Component
import kotlinx.coroutines.CoroutineDispatcher

@AuthFeatureScope
@Component(
    modules = [AuthFeatureModule::class],
    dependencies = [AuthFeatureDependencies::class]
)
internal interface AuthFeatureComponent : AuthFeatureApi {
    fun inject(authFragment: AuthFragment)
}

interface AuthFeatureApi {
    fun provideAuthFeatureInteractor(): AuthFeatureInteractor
}

interface AuthFeatureDependencies {
    fun networkAuthApi(): AuthApi
    fun networkInteractor(): NetworkInteractor
    fun persistentPreferencesApi(): Preferences
    @IoDispatcher fun dispatcherIO(): CoroutineDispatcher
}