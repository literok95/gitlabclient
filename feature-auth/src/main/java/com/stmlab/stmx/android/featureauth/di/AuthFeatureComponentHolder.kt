package com.stmlab.stmx.android.featureauth.di

import android.util.Log

object AuthFeatureComponentHolder {
    private var authFeatureComponentHolder: AuthFeatureComponent? = null

    fun init(dependencies: AuthFeatureDependencies) {
        if (authFeatureComponentHolder == null) {
            synchronized(AuthFeatureComponentHolder::class.java) {
                if (authFeatureComponentHolder == null) {
                    Log.i("PurchaseCH", "initAndGet()")
                    authFeatureComponentHolder = DaggerAuthFeatureComponent.builder()
                        .authFeatureDependencies(dependencies)
                        .build()
                }
            }
        }
    }

    fun get(): AuthFeatureApi {
        return getComponent()
    }

    internal fun getComponent(): AuthFeatureComponent {
        return checkNotNull(authFeatureComponentHolder) { "AuthFeatureComponent was not initialized!" }
    }

    fun reset() {
        authFeatureComponentHolder = null
    }
}