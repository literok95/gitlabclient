package com.stmlab.stmx.android.featureauth.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.stmlab.stmx.android.core.scopes.AuthFeatureScope
import com.stmlab.stmx.android.featureauth.AuthFeatureInteractor
import com.stmlab.stmx.android.featureauth.AuthFeatureInteractorImpl
import com.stmlab.stmx.android.featureauth.ui.AuthViewModel
import dagger.Binds
import dagger.Module

@Module
internal interface AuthFeatureModule {

    @Binds
    @AuthFeatureScope
    fun provideViewModelFactory(viewModelProvider: AuthViewModel.ViewModelFactory): ViewModelProvider.Factory

    @Binds
    fun provideViewModel(viewModel: AuthViewModel): ViewModel

    @Binds
    @AuthFeatureScope
    fun provideAuthFeatureInteractor(authFeatureInteractor: AuthFeatureInteractorImpl): AuthFeatureInteractor

}