package com.stmlab.stmx.android.featureauth.models

sealed class AuthState {
    class OAuthUrlLoading(val url: String) : AuthState()
    object AccessTokenChecking: AuthState()
    object AuthSuccess: AuthState()
    object AuthFailure: AuthState()
}
