package com.stmlab.stmx.android.featureauth.models

import com.stmlab.stmx.android.corenetwork.models.TokensDto

fun TokensDto.toTokens(): Tokens {
    return Tokens(
        checkNotNull(this.accessToken) { "accessToken is null" },
        checkNotNull(this.refreshToken) { "refreshToken is null" }
    )
}