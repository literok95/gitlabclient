package com.stmlab.stmx.android.featureauth.models

data class Tokens(
    val accessToken: String,
    val refreshToken: String
)