package com.stmlab.stmx.android.featureauth.repository

import com.orhanobut.logger.Logger
import com.stmlab.stmx.android.core.BaseRepository
import com.stmlab.stmx.android.core.BaseResult
import com.stmlab.stmx.android.core.di.IoDispatcher
import com.stmlab.stmx.android.core.models.Account
import com.stmlab.stmx.android.core.models.AuthConfig
import com.stmlab.stmx.android.core.scopes.AuthFeatureScope
import com.stmlab.stmx.android.featureauth.models.Tokens
import com.stmlab.stmx.android.featureauth.models.toTokens
import com.stmlab.stmx.android.corenetwork.AuthApi
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.models.TokensDto
import com.stmlab.stmx.android.corepersistent.Preferences
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import javax.inject.Inject

@AuthFeatureScope
internal class AuthRepository @Inject constructor(
    private val authApi: AuthApi,
    private val preferences: Preferences,
    private val networkInteractor: NetworkInteractor,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : BaseRepository(dispatcher) {

    private var onAuthSuccessListener: (() -> Unit)? = null

    fun setOnAuthSuccessListener(onAuthSuccessListener: (() -> Unit)?) {
        this.onAuthSuccessListener = onAuthSuccessListener
    }

    suspend fun getAccessToken(
        authConfig: AuthConfig,
        code: String,
    ): BaseResult<Tokens> {
        Logger.t("AUTH_REPO")
        Logger.d("set new host: ${authConfig.gitHost}")
        networkInteractor.setHost(authConfig.gitHost)
        val tokens = makeRequest(TokensDto::toTokens) {
            authApi.getAccessToken(
                authConfig.appId, authConfig.clientSecret,
                code, "authorization_code",
                authConfig.callbackUri
            )
        }
        if (tokens is BaseResult.Success) {
            networkInteractor.setAccessToken(tokens.body.accessToken, true)
            preferences.currentAccount = Account(
                tokens.body.accessToken, tokens.body.refreshToken, authConfig.gitHost, true
            )
            onAuthSuccessListener?.invoke()
        }
        return tokens
    }

    fun signOut() {
        Logger.t("AUTH_REPO")
        Logger.d("sign out")
        networkInteractor.setDefaultGitHost()
        networkInteractor.clearAccessToken()
        preferences.currentAccount = null
    }

    fun checkAuth(): Boolean {
        return preferences.currentAccount?.let { account ->
            Logger.t("AUTH_REPO")
            Logger.d("set saved host: ${account.host}")
            networkInteractor.setAccessToken(account.accessToken, account.isOAuth)
            networkInteractor.setHost(account.host)
            true
        } ?: false
    }

    fun authSuccess() {
        onAuthSuccessListener?.invoke()
    }
}