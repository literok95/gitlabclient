package com.stmlab.stmx.android.featureauth.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModelProvider
import com.orhanobut.logger.Logger
import com.stmlab.stmx.android.core.scopes.AuthFeatureScope
import com.stmlab.stmx.android.featureauth.databinding.FragmentAuthBinding
import com.stmlab.stmx.android.featureauth.di.AuthFeatureComponentHolder
import com.stmlab.stmx.android.featureauth.models.AuthState
import dagger.Lazy
import javax.inject.Inject

@AuthFeatureScope
internal class AuthFragment : Fragment(), LifecycleObserver {

    @Inject
    lateinit var factory: Lazy<ViewModelProvider.Factory>

    private var _binding: FragmentAuthBinding? = null
    private val binding get() = checkNotNull(_binding) { "FragmentAuthBinding not init" }

    private val viewModel: AuthViewModel by viewModels { factory.get() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Logger.t("AUTH_FRAGMENT")
        Logger.d("onCreateView")
        _binding = FragmentAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Logger.t("AUTH_FRAGMENT")
        Logger.d("onViewCreated")
        viewLifecycleOwner.lifecycle.addObserver(this)
        AuthFeatureComponentHolder.getComponent().inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun bindViews() {
        with(binding) {
            buttonSignInViaPassword.setOnClickListener {
                viewModel.startOAuth(getGitHost())
            }
            buttonSignOut.setOnClickListener {
                signOut(viewModel)
            }
            buttonSignInViaAccessToken.setOnClickListener {
                viewModel.authSuccess()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun setViewModelObserver() {
        viewModel.authState.observe(viewLifecycleOwner) { authState ->
            when (authState) {
                is AuthState.AccessTokenChecking -> startAccessTokenChecking()
                is AuthState.AuthSuccess -> viewModel.authSuccess()
                is AuthState.OAuthUrlLoading -> startOAuthLoading(authState.url)
                is AuthState.AuthFailure -> setInitialStateView()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun setInitialStateView() {
        binding.webView.isVisible = false
    }

    @SuppressLint("SetJavaScriptEnabled")
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun initWebView() {
        CookieManager.getInstance().setAcceptCookie(true)
        with(binding.webView) {
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            settings.textZoom = 100
            settings.userAgentString = requireContext().packageName
            webViewClient = WebViewClientImpl { uri -> viewModel.shouldOverrideUrl(uri) }
            setLayerType(View.LAYER_TYPE_HARDWARE, null)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun clearBind() {
        _binding = null
    }

    private fun startOAuthLoading(url: String) {
        binding.webView.isVisible = true
        binding.webView.loadUrl(url)
    }

    private fun startAccessTokenChecking() {
        binding.webView.isVisible = false
    }

    private fun signOut(viewModel: AuthViewModel) {
        CookieManager.getInstance().removeAllCookies(null)
        viewModel.signOut()
        setInitialStateView()
    }

    private fun getGitHost(): String {
        return binding.editTextHost.text.toString()
    }

}