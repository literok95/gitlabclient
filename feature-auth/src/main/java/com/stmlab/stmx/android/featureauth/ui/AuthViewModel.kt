package com.stmlab.stmx.android.featureauth.ui

import android.net.Uri
import androidx.lifecycle.*
import com.orhanobut.logger.Logger
import com.stmlab.stmx.android.core.BaseResult
import com.stmlab.stmx.android.core.models.AuthConfig
import com.stmlab.stmx.android.core.scopes.AuthFeatureScope
import com.stmlab.stmx.android.featureauth.BuildConfig
import com.stmlab.stmx.android.featureauth.models.AuthState
import com.stmlab.stmx.android.featureauth.repository.AuthRepository
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

internal class AuthViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : ViewModel() {
    private var _authState: MutableLiveData<AuthState> = MutableLiveData()
    val authState: LiveData<AuthState> = _authState

    private var authConfig: AuthConfig? = null

    init {
        Logger.t("AUTH_VM")
        Logger.d("init")
    }

    fun shouldOverrideUrl(uri: Uri?): Boolean {
        Logger.t("AUTH_VM")
        Logger.d("shouldOverrideUrl: $uri")
        val authConfig = checkNotNull(authConfig) { "AuthConfig not initialize" }
        val isAppRedirect = isAppRedirect(uri)
        if (isAppRedirect) getAccessToken(authConfig, uri)
        return isAppRedirect
    }

    fun startOAuth(gitHost: String) {
        Logger.t("AUTH_VM")
        Logger.d("startOAuth")
        val authConfig = createOAuthConfig(gitHost)
        this.authConfig = authConfig
        _authState.postValue(AuthState.OAuthUrlLoading(authConfig.toUrl()))
    }

    fun signOut() {
        Logger.t("AUTH_VM")
        Logger.d("signOut")
        authRepository.signOut()
    }

    private fun getAccessToken(authConfig: AuthConfig, uri: Uri?) {
        Logger.t("AUTH_VM")
        Logger.d("getAccessToken")
        viewModelScope.launch {
            _authState.postValue(AuthState.AccessTokenChecking)
            val result = authRepository.getAccessToken(authConfig, uri.getCode())
            Logger.t("AUTH_VM")
            Logger.d("result: $result")
            if (result is BaseResult.Success) {
                _authState.postValue(AuthState.AuthSuccess)
            }
        }
    }

    private fun isAppRedirect(uri: Uri?): Boolean {
        val callbackUri = BuildConfig.CALLBACK_URI
        val isAppRedirect = uri.toString().indexOf(callbackUri) == 0
        Logger.t("AUTH_VM")
        Logger.d("isAppRedirect: $isAppRedirect")
        return isAppRedirect

    }

    private fun Uri?.getCode(): String {
        return this?.getQueryParameter("code") ?: ""
    }

    private fun createOAuthConfig(host: String): AuthConfig {
        return AuthConfig(
            host,
            BuildConfig.APP_ID,
            BuildConfig.APP_SECRET,
            BuildConfig.CALLBACK_URI,
            UUID.randomUUID().toString().hashCode(),
            BuildConfig.SCOPES
        )
    }

    fun authSuccess() {
        authRepository.authSuccess()
    }

    class ViewModelFactory @Inject constructor(
        private val viewModelProvider: Provider<ViewModel>
    ) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return viewModelProvider.get() as T
        }
    }
}