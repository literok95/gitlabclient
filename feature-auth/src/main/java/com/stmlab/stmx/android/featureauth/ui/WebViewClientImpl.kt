package com.stmlab.stmx.android.featureauth.ui

import android.net.Uri
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

internal class WebViewClientImpl(var overrideUrlListener: ((Uri?) -> Boolean)?) : WebViewClient() {
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return overrideUrlListener?.invoke(request?.url) ?: false
    }
}