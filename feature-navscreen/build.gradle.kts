plugins {
    androidLibrary()
    kotlinAndroid()
    kapt()
}

android {
    compileSdk = AppConfig.sdkVersion
    buildToolsVersion = AppConfig.buildToolsVersion

    defaultConfig {
        minSdk = AppConfig.minSdkVersion
        targetSdk = AppConfig.sdkVersion
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }
}

dependencies {
//    implementation(project(":core"))
    androidX()
    ui()
    fragment()
    lifecycle()
    navigation()
    dagger()
    leakCanary()
    tests()
}