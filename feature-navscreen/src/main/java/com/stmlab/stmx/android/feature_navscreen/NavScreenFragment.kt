package com.stmlab.stmx.android.feature_navscreen

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class NavScreenFragment : Fragment(R.layout.frgament_navscreen) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomNavigationView =
            view.findViewById<BottomNavigationView>(R.id.bottom_navigation_view)
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.nested_nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        val menuResId = arguments?.getInt("MENU_RES_ID_TAG") ?: -1
        val navGraphResId = arguments?.getInt("BOTTOM_NAV_GRAPH_RES_ID_TAG") ?: -1
        setNavGraph(navController, bottomNavigationView, navGraphResId)
        inflateBottomNavMenu(bottomNavigationView, menuResId)
    }

    private fun setNavGraph(
        navController: NavController,
        bottomNavigationView: BottomNavigationView,
        navGraphResId: Int
    ) {
        if (navGraphResId != -1) {
            navController.setGraph(navGraphResId)
            bottomNavigationView.setupWithNavController(navController)
        }
    }

    private fun inflateBottomNavMenu(bottomNavigationView: BottomNavigationView, menuResId: Int) {
        if (menuResId == -1) {
            bottomNavigationView.isVisible = false
        } else {
            bottomNavigationView.isVisible = true
            bottomNavigationView.inflateMenu(menuResId)
        }
    }

}