package com.stmlab.stmx.android.featureuser

import com.stmlab.stmx.android.core.scopes.UserFeatureScope
import com.stmlab.stmx.android.featureuser.repository.UserFeatureRepository
import javax.inject.Inject

@UserFeatureScope
internal class UserFeatureInteractorImpl @Inject constructor(
    private val repository: UserFeatureRepository
) : UserFeatureInteractor {
    override fun addSignOutListener(callback: (() -> Unit)?) {
        repository.addSignOutListener(callback)
    }
}

interface UserFeatureInteractor {
    fun addSignOutListener(callback: (() -> Unit)?)
}