package com.stmlab.stmx.android.featureuser.di

import com.stmlab.stmx.android.core.di.IoDispatcher
import com.stmlab.stmx.android.core.scopes.UserFeatureScope
import com.stmlab.stmx.android.featureuser.UserFeatureInteractor
import com.stmlab.stmx.android.featureuser.ui.UserFeatureFragment
import com.stmlab.stmx.android.featureuser.ui.UserFeatureViewModel
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.UserApi
import dagger.Component
import kotlinx.coroutines.CoroutineDispatcher

@UserFeatureScope
@Component(
    modules = [UserFeatureModule::class],
    dependencies = [UserFeatureDependencies::class]
)
internal interface UserFeatureComponent : UserFeatureApi {
    fun inject(fragment: UserFeatureFragment)
    fun inject(viewModel: UserFeatureViewModel)
}

interface UserFeatureDependencies {
    fun networkUserApi(): UserApi
    fun networkInteractor(): NetworkInteractor
    @IoDispatcher
    fun dispatcherIO(): CoroutineDispatcher
}

interface UserFeatureApi {
    fun provideUserFeatureInteractor(): UserFeatureInteractor
}