package com.stmlab.stmx.android.featureuser.di

import android.util.Log

object UserFeatureComponentHolder {
    private var userFeatureComponentHolder: UserFeatureComponent? = null

    fun init(userFeatureDependencies: UserFeatureDependencies) {
        if (userFeatureComponentHolder == null) {
            synchronized(UserFeatureComponentHolder::class.java) {
                if (userFeatureComponentHolder == null) {
                    Log.i("PurchaseCH", "initAndGet()")
                    userFeatureComponentHolder = DaggerUserFeatureComponent.builder()
                        .userFeatureDependencies(userFeatureDependencies)
                        .build()
                }
            }
        }
    }

    fun get(): UserFeatureApi {
        return getComponent()
    }

    internal fun getComponent(): UserFeatureComponent {
        return checkNotNull(userFeatureComponentHolder) { "UserFeatureComponentHolder was not initialized!" }
    }

    fun reset() {
        userFeatureComponentHolder = null
    }
}