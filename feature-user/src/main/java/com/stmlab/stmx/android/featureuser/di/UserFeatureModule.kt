package com.stmlab.stmx.android.featureuser.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.stmlab.stmx.android.core.scopes.UserFeatureScope
import com.stmlab.stmx.android.featureuser.UserFeatureInteractor
import com.stmlab.stmx.android.featureuser.UserFeatureInteractorImpl
import com.stmlab.stmx.android.featureuser.ui.UserFeatureViewModel
import dagger.Binds
import dagger.Module

@Module
internal interface UserFeatureModule {

    @Binds
    @UserFeatureScope
    fun provideViewModelFactory(viewModelProvider: UserFeatureViewModel.ViewModelFactory): ViewModelProvider.Factory

    @Binds
    fun provideViewModel(viewModel: UserFeatureViewModel): ViewModel

    @Binds
    @UserFeatureScope
    fun provideUserFeatureInteractor(UserFeatureInteractor: UserFeatureInteractorImpl): UserFeatureInteractor
}