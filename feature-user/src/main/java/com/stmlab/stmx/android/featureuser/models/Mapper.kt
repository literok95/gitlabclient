package com.stmlab.stmx.android.featureuser.models

import com.stmlab.stmx.android.corenetwork.models.UserDto

internal fun UserDto.toUser(): User {
    return User(
        id = checkNotNull(this.id){"User id is not defiend"},
        avatarUrl = checkNotNull(this.avatarUrl){"Avatar is not defiend"},
        name = checkNotNull(this.name){"name is not defiend"},
        userName = checkNotNull(this.username){"UserName is not defiend"},
        email = checkNotNull(this.email){"email is not defiend"},
        location = this.location
    )
}