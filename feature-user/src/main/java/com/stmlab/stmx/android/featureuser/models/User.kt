package com.stmlab.stmx.android.featureuser.models

internal data class User(
    val id: Int,
    val avatarUrl: String,
    val name: String,
    val userName: String,
    val email: String,
    val location: String?
)