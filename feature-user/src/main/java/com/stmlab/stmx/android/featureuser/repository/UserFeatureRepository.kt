package com.stmlab.stmx.android.featureuser.repository

import com.stmlab.stmx.android.core.BaseRepository
import com.stmlab.stmx.android.core.BaseResult
import com.stmlab.stmx.android.core.di.IoDispatcher
import com.stmlab.stmx.android.core.scopes.UserFeatureScope
import com.stmlab.stmx.android.corenetwork.NetworkInteractor
import com.stmlab.stmx.android.corenetwork.UserApi
import com.stmlab.stmx.android.corenetwork.models.UserDto
import com.stmlab.stmx.android.featureuser.models.User
import com.stmlab.stmx.android.featureuser.models.toUser
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@UserFeatureScope
internal class UserFeatureRepository @Inject constructor(
    private val userApi: UserApi,
    private val networkInteractor: NetworkInteractor,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseRepository(dispatcher) {

    private var signOutListener: (() -> Unit)? = null

    fun addSignOutListener(callback: (() -> Unit)?) {
        this.signOutListener = callback
    }

    fun clearSignOutListener() {
        signOutListener = null
    }

    fun signOut() {
        signOutListener?.invoke()
    }

    suspend fun getCurrentUser(): BaseResult<User> {
        return makeRequest(UserDto::toUser) {
            userApi.getCurrentUser()
        }
    }
}