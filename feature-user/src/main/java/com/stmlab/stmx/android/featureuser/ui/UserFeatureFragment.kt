package com.stmlab.stmx.android.featureuser.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.stmlab.stmx.android.core.BaseResult
import com.stmlab.stmx.android.featureuser.R
import com.stmlab.stmx.android.featureuser.databinding.FragmentUserBinding
import com.stmlab.stmx.android.featureuser.di.UserFeatureComponentHolder
import com.stmlab.stmx.android.featureuser.models.User
import dagger.Lazy
import javax.inject.Inject

internal class UserFeatureFragment : Fragment() {

    @Inject
    lateinit var factory: Lazy<ViewModelProvider.Factory>

    private var _binding: FragmentUserBinding? = null
    private val binding get() = checkNotNull(_binding) { "FragmentUserBinding not init" }

    private val viewModel: UserFeatureViewModel by viewModels { factory.get() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d("TagMyTagFragment", "onCreateView savedState:$savedInstanceState")
        UserFeatureComponentHolder.getComponent().inject(this)
        _binding = FragmentUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("TagMyTagFragment", "onViewCreated savedState:$savedInstanceState")
        initViews()
        binding.signOutButton.setOnClickListener {
            viewModel.signOut()
        }
        viewModel.userLiveData.observe(viewLifecycleOwner) { result ->
            when (result) {
                is BaseResult.Error -> Toast.makeText(requireContext(), "Error", Toast.LENGTH_LONG).show()
                is BaseResult.Success -> bindSuccessResult(result.body)
            }
        }
        viewModel.getUser()
    }

    private fun initViews() {
        with(binding) {
            nameTextView.isVisible = false
            usernameTextView.isVisible = false
            emailTextView.isVisible = false
            locationTextView.isVisible = false
        }
    }

    private fun bindSuccessResult(user: User) {
        with(binding) {
            nameTextView.text = user.name
            usernameTextView.text = getString(R.string.username_placeholder, user.userName)
            emailTextView.text = user.email
            if (user.location != null) {
                locationTextView.isVisible = true
                locationTextView.text = user.location
            } else {
                locationTextView.isVisible = false
            }
            nameTextView.isVisible = true
            usernameTextView.isVisible = true
            emailTextView.isVisible = true
            Glide.with(this@UserFeatureFragment)
                .load(user.avatarUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(avatarImageView)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("TagMyTagFragment", "onDestroyView")
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        Log.d("TagMyTagFragment", "onStart")
    }

    override fun onStop() {
        super.onStop()
        Log.d("TagMyTagFragment", "onStop")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d("TagMyTagFragment", "onAttach")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d("TagMyTagFragment", "onDetach")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TagMyTagFragment", "onDestroy")
    }

    override fun onResume() {
        super.onResume()
        Log.d("TagMyTagFragment", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("TagMyTagFragment", "onPause")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        Log.d("TagMyTagFragment", "onViewStateRestored savedState:$savedInstanceState")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d("TagMyTagFragment", "onSaveInstanceState")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("TagMyTagFragment", "onCreate savedState:$savedInstanceState")
    }
}