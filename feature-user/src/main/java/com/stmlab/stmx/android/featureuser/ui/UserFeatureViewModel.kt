package com.stmlab.stmx.android.featureuser.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.stmlab.stmx.android.core.BaseResult
import com.stmlab.stmx.android.featureuser.models.User
import com.stmlab.stmx.android.featureuser.repository.UserFeatureRepository
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

internal class UserFeatureViewModel @Inject constructor(
    private val repository: UserFeatureRepository
) : ViewModel() {

    private var _liveData: MutableLiveData<BaseResult<User>> = MutableLiveData()
    val userLiveData: LiveData<BaseResult<User>> = _liveData

    fun signOut() {
        repository.signOut()
    }

    fun getUser() {
        viewModelScope.launch {
            val result = repository.getCurrentUser()
            _liveData.postValue(result)
        }
    }

    class ViewModelFactory @Inject constructor(
        private val viewModelProvider: Provider<ViewModel>
    ) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return viewModelProvider.get() as T
        }
    }
}